package inheritance;

public class BWM extends Car{ // Called "has-a" relationship
	
	public void start() { // Method-Overriding
		System.out.println("BMW--start");
	}
	
	public void theftSafty() {
		System.out.println("BMW--theft safty");
	}

}
