package inheritance;

public class TestCar {
	
	public static void main(String[] args) {
		
		/* Called "static-polymorphism" OR "compile-time polymorphism (Cause 
		 * during compile-time java will decide what method to call.)*/
		BWM b = new BWM();
		b.start();
		b.stop();
		b.refuel();
		b.theftSafty();
		
		System.out.println(" ");
		
		Car c = new Car();
		c.start();
		c.stop();
		c.refuel();
		
		System.out.println(" ");
		
		/* Called "dynamic-polymorphism" OR "run-time polymorphism" (Means child class object
		 * can be referred by parent class reference variable)*/
		Car c1 = new BWM();
		c1.start();
		c1.stop();
		c1.refuel();
	}
}
