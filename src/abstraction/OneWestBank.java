package abstraction;

public interface OneWestBank {
	
//Variables
	
	int min_balance = 100;
	
//Methods
	public void credit();
	
	void debit();

}
