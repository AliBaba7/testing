package abstraction;

public class MyBank extends Chase implements OneWestBank{

	@Override
	public void credit() {
		System.out.println("This is credit.");
		
	}

	@Override
	public void debit() {
		System.out.println("This is debit.");
		
	}

	@Override
	public void studentLoan() {
		System.out.println("This is student loan.");
		
	}
	
	public static void main(String[] args) {
		
		System.out.println(min_balance);
		
		MyBank b = new MyBank();
		System.out.println(min_balance);
		System.out.println(b.account_fee);
		b.credit();
		b.debit();
		b.homeLoan();
		b.carLoan();
		b.studentLoan();
		
		System.out.println(" ");
		
		// Dynamic Polymorphism --> child class object can be referred by Interface reference variable
		Chase c = new MyBank();
		System.out.println(min_balance);
		System.out.println(b.account_fee);
		c.homeLoan();
		c.carLoan();
		c.studentLoan();
	}
	
}
