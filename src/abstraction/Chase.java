package abstraction;

public abstract class Chase {
	
//Variables
	
	int account_fee = 200;
	
//Methods
	public void homeLoan() {
		System.out.println("This is home loan.");
	}
	
	void carLoan() {
		System.out.println("This is car loan.");
	}
	
	public abstract void studentLoan();

}
